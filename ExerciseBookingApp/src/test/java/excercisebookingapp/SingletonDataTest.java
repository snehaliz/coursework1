package excercisebookingapp;

import exercisebookingapp.data.SingletonData;
import exercisebookingapp.model.Timetable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class SingletonDataTest {

    private static SingletonData singletonData;
    private int bookingId = -1;

   @BeforeClass
    public static void setUp() {
       System.out.println("SingletonDataTest is setting up. Creating singletonData instance.");
        singletonData = SingletonData.getInstance();
    }

    @Test
    public void addValidBooking() {
        int studentId = singletonData.getStudentList().get(0).getStudentId();
        int lessonId = singletonData.getTimetableList().get(0).getLessonId();
        int newBookingId = singletonData.addToBooking(studentId, lessonId);
        System.out.println("Test should successfully add a booking with id: " + newBookingId);
        assertEquals("Test should successfully add a booking with id: " + newBookingId, newBookingId, singletonData.getBookingId());
        bookingId = newBookingId;
    }

    @Test
    public void addInvalidBooking() {
        int studentId = singletonData.getStudentList().get(0).getStudentId();
        int lessonId = singletonData.getTimetableList().get(0).getLessonId();
        int newBookingId = singletonData.addToBooking(studentId, lessonId);
        System.out.println("Test should not create a new booking with same student id and same lesson id");
        assertNotEquals("Test should successfully create a booking for the first time", newBookingId, -1);
        assertEquals("Test should not create a new booking with same student id and same lesson id", singletonData.addToBooking(studentId, lessonId), -1);
        bookingId = newBookingId;
    }

    @Test
    public void cancelBookingSuccessfully() {
       addValidBooking();
       System.out.println("After adding a valid booking, test should successfully cancel the same booking");
       assertTrue("After adding a valid booking, test should successfully cancel the same booking", singletonData.cancelBooking(bookingId));
       bookingId = -1;
    }

    @Test
    public void cancelBookingShouldFail() {
       int dummyBookingID = Integer.MIN_VALUE;
       System.out.println("Test should return false when attempted to cancel an invalid booking");
       assertFalse("Test should return false when attempted to cancel an invalid booking", singletonData.cancelBooking(dummyBookingID));
    }

    @Test
    public void addRatingSuccessfully() {
       int lessonId = singletonData.getTimetableList().get(0).getLessonId();
       int rating = 5;
       singletonData.addRating(lessonId, rating);
       int updatedRating = singletonData.getRatingData()
               .get(lessonId)
               .get(singletonData.getRatingData().size() - 1);
       System.out.println("Test should successfully update new rating for exercise");
       assertNotEquals("Test should successfully update new rating for exercise ", rating, updatedRating);
    }

    @Test
    public void isValidBookingSuccess() {
       addValidBooking();
       System.out.println("Test should return true for valid booking id");
       assertTrue("Test should return true for valid booking id", singletonData.isValidBookingID(bookingId));
    }

    @Test
    public void isValidBookingFailed() {
        System.out.println("Test should return true for valid booking id");
        assertFalse("Test should return true for valid booking id", singletonData.isValidBookingID(Integer.MIN_VALUE));
    }

    @Test
    public void getLessonSuccessfully() {
       int lessonId = singletonData.getTimetableList().get(0).getLessonId();
       Timetable actualTimetable = singletonData.getTimetableList().get(0);
       System.out.println("Test should be successful in comparing the timetables.");
       assertEquals("Test should be successful in comparing the timetables.", actualTimetable, singletonData.getLesson(lessonId));
    }

    @Test
    public void getBkdStudentIdSuccessfully() {
       addValidBooking();
       int studentId = singletonData.getStudentList().get(0).getStudentId();
       System.out.println("Test should successfully compare both expected student id and computed student id");
       assertEquals("Test should successfully compare both expected student id and computed student id", studentId, singletonData.getBkdStudentId(bookingId));
    }

    @Test(expected = NullPointerException.class)
    public void getBkdStudentIdException() {
       System.out.println("test should throw Null pointer exception when querying for a booked student id with an invalid booking id");
       singletonData.getBkdStudentId(Integer.MIN_VALUE);
    }

    @Test
    public void modifyBookingSuccessfully() {
       addValidBooking();
       int studentId = singletonData.getStudentList().get(0).getStudentId();
       int lessonId = singletonData.getTimetableList().get(0).getLessonId();
       System.out.println("Test should successfully modify an existing booking with new student id and lesson id");
       assertNotEquals("Test should successfully modify an existing booking with new student id and lesson id", 1, singletonData.modifyBooking(bookingId, studentId, lessonId));
    }

    @Test
    public void modifyBookingForInvalidBookingID() {
       System.out.println("Test should not modify booking for an invalid booking id");
       assertNotEquals("Test should not modify booking for an invalid booking id", -1, singletonData.modifyBooking(bookingId, Integer.MIN_VALUE, Integer.MIN_VALUE));
    }

    @After
    public void cleanUpBooking() {
        if (bookingId != -1) {
            assertTrue("Clean up should successfully remove any new booking made by test", singletonData.cancelBooking(bookingId));
        }
    }

    @AfterClass
    public static void destroy() {
       singletonData = null;
    }

}
