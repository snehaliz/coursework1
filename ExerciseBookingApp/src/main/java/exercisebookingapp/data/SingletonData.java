/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package exercisebookingapp.data;

import exercisebookingapp.model.Student;
import exercisebookingapp.model.Timetable;

import java.util.*;
import java.util.stream.Collectors;

import static exercisebookingapp.data.SingletonData.Day.SATURDAY;
import static exercisebookingapp.data.SingletonData.Day.SUNDAY;
import static exercisebookingapp.data.SingletonData.Exercise.*;
import static exercisebookingapp.data.SingletonData.Slot.*;

/**
 * @author Snehajeffy
 */
public class SingletonData {

    int bookingId = 2000;
    private static SingletonData singletonData;

    private ArrayList<Student> studentList = new ArrayList<>();
    private ArrayList<Timetable> timetableList = new ArrayList<>();

    private HashMap<Integer, ArrayList<Integer>> bookingData = new HashMap<>();

    private HashMap<Integer, ArrayList<Integer>> ratingData = new HashMap<>();
    private HashMap<Integer, ArrayList<String>> reviewData = new HashMap<>();
    private HashMap<String, Double> price = new HashMap<>();

    private SingletonData() {
        loadTimetable();
        addStudent();
        addPrice();
        addDummyRatings();
    }

    private void addDummyRatings() {
        ratingData.put(100,new ArrayList<Integer>(Arrays.asList(1,2,4,5,3,2,1,5,4,2,3)));
        ratingData.put(101,new ArrayList<Integer>(Arrays.asList(1,2,4,5,3,2,1,4,2,3)));
        ratingData.put(100,new ArrayList<Integer>(Arrays.asList(1,2,4,5,3,2,1,5,4,2,3)));
        ratingData.put(140,new ArrayList<Integer>(Arrays.asList(1,2,4,5,2,1,5,5,5,2,5,2,3)));
        ratingData.put(141,new ArrayList<Integer>(Arrays.asList(1,2,4,5,3,2,1,5,4,2,3)));
        ratingData.put(142,new ArrayList<Integer>(Arrays.asList(1,2,4,4,5,5,3,1,2,5,2,1,5,4,2,3)));
    }

    private void addPrice() {
        price.put(YOGA.toString(), 15.00);
        price.put(ZUMBA.toString(), 18.23);
        price.put(AQUACISE.toString(), 5.50);
        price.put(BOX_FIT.toString(), 6.00);
        price.put(BODY_BLITZ.toString(), 19.23);
    }

    public static SingletonData getInstance() {
        if (singletonData == null) {
            singletonData = new SingletonData();
        }

        return singletonData;
    }

    public void loadTimetable() {

        timetableList.add(new Timetable(100, 1, 1, SATURDAY, MORNING, YOGA, (float) 15.00));
        timetableList.add(new Timetable(101, 1, 1, SATURDAY, AFTERNOON, ZUMBA, (float) 18.23));
        timetableList.add(new Timetable(102, 1, 1, SATURDAY, EVENING, BOX_FIT, (float) 6.00));
        timetableList.add(new Timetable(103, 1, 1, SUNDAY, MORNING, YOGA, (float) 15.00));
        timetableList.add(new Timetable(104, 1, 1, SUNDAY, AFTERNOON, AQUACISE, (float) 5.50));
        timetableList.add(new Timetable(105, 1, 1, SUNDAY, EVENING, BODY_BLITZ, (float) 19.23));
        timetableList.add(new Timetable(106, 1, 2, SATURDAY, MORNING, YOGA, (float) 15.00));
        timetableList.add(new Timetable(107, 1, 2, SATURDAY, AFTERNOON, ZUMBA, (float) 18.23));
        timetableList.add(new Timetable(108, 1, 2, SATURDAY, EVENING, BOX_FIT, (float) 6.00));
        timetableList.add(new Timetable(109, 1, 2, SUNDAY, MORNING, YOGA, (float) 15.00));
        timetableList.add(new Timetable(110, 1, 2, SUNDAY, AFTERNOON, AQUACISE, (float) 5.50));
        timetableList.add(new Timetable(111, 1, 2, SUNDAY, EVENING, BODY_BLITZ, (float) 19.23));
        timetableList.add(new Timetable(112, 1, 3, SATURDAY, MORNING, YOGA, (float) 15.00));
        timetableList.add(new Timetable(113, 1, 3, SATURDAY, AFTERNOON, ZUMBA, (float) 18.23));
        timetableList.add(new Timetable(114, 1, 3, SATURDAY, EVENING, BOX_FIT, (float) 6.00));
        timetableList.add(new Timetable(115, 1, 3, SUNDAY, MORNING, YOGA, (float) 15.00));
        timetableList.add(new Timetable(116, 1, 3, SUNDAY, AFTERNOON, AQUACISE, (float) 5.50));
        timetableList.add(new Timetable(117, 1, 3, SUNDAY, EVENING, BODY_BLITZ, (float) 19.23));
        timetableList.add(new Timetable(118, 1, 4, SATURDAY, MORNING, YOGA, (float) 15.00));
        timetableList.add(new Timetable(119, 1, 4, SATURDAY, AFTERNOON, ZUMBA, (float) 18.23));
        timetableList.add(new Timetable(120, 1, 4, SATURDAY, EVENING, BOX_FIT, (float) 6.00));
        timetableList.add(new Timetable(121, 1, 4, SUNDAY, MORNING, YOGA, (float) 15.00));
        timetableList.add(new Timetable(122, 1, 4, SUNDAY, AFTERNOON, AQUACISE, (float) 5.50));
        timetableList.add(new Timetable(123, 1, 4, SUNDAY, EVENING, BODY_BLITZ, (float) 19.23));
        timetableList.add(new Timetable(124, 2, 1, SATURDAY, MORNING, YOGA, (float) 15.00));
        timetableList.add(new Timetable(125, 2, 1, SATURDAY, AFTERNOON, ZUMBA, (float) 18.23));
        timetableList.add(new Timetable(126, 2, 1, SATURDAY, EVENING, BOX_FIT, (float) 6.00));
        timetableList.add(new Timetable(127, 2, 1, SUNDAY, MORNING, YOGA, (float) 15.00));
        timetableList.add(new Timetable(128, 2, 1, SUNDAY, AFTERNOON, AQUACISE, (float) 5.50));
        timetableList.add(new Timetable(129, 2, 1, SUNDAY, EVENING, BODY_BLITZ, (float) 19.23));
        timetableList.add(new Timetable(130, 2, 2, SATURDAY, MORNING, YOGA, (float) 15.00));
        timetableList.add(new Timetable(131, 2, 2, SATURDAY, AFTERNOON, ZUMBA, (float) 18.23));
        timetableList.add(new Timetable(132, 2, 2, SATURDAY, EVENING, BOX_FIT, (float) 6.00));
        timetableList.add(new Timetable(133, 2, 2, SUNDAY, MORNING, YOGA, (float) 15.00));
        timetableList.add(new Timetable(134, 2, 2, SUNDAY, AFTERNOON, AQUACISE, (float) 5.50));
        timetableList.add(new Timetable(135, 2, 2, SUNDAY, EVENING, BODY_BLITZ, (float) 19.23));
        timetableList.add(new Timetable(136, 2, 3, SATURDAY, MORNING, YOGA, (float) 15.00));
        timetableList.add(new Timetable(137, 2, 3, SATURDAY, AFTERNOON, ZUMBA, (float) 18.23));
        timetableList.add(new Timetable(138, 2, 3, SATURDAY, EVENING, BOX_FIT, (float) 6.00));
        timetableList.add(new Timetable(139, 2, 3, SUNDAY, MORNING, YOGA, (float) 15.00));
        timetableList.add(new Timetable(140, 2, 3, SUNDAY, AFTERNOON, AQUACISE, (float) 5.50));
        timetableList.add(new Timetable(141, 2, 3, SUNDAY, EVENING, BODY_BLITZ, (float) 19.23));
        timetableList.add(new Timetable(142, 2, 4, SATURDAY, MORNING, YOGA, (float) 15.00));
        timetableList.add(new Timetable(143, 2, 4, SATURDAY, AFTERNOON, ZUMBA, (float) 18.23));
        timetableList.add(new Timetable(144, 2, 4, SATURDAY, EVENING, BOX_FIT, (float) 6.00));
        timetableList.add(new Timetable(145, 2, 4, SUNDAY, MORNING, YOGA, (float) 15.00));
        timetableList.add(new Timetable(146, 2, 4, SUNDAY, AFTERNOON, AQUACISE, (float) 5.50));
        timetableList.add(new Timetable(147, 2, 4, SUNDAY, EVENING, BODY_BLITZ, (float) 19.23));

    }

    public void addStudent() {
        studentList.add(new Student(1000, "John Michael", "07485961425"));
        studentList.add(new Student(1001, "Edward Thomas", "07485961425"));
        studentList.add(new Student(1002, "Esther James", "07485961445"));
        studentList.add(new Student(1003, "Ann Francis", "07485861425"));
        studentList.add(new Student(1004, "Kelly Bowen", "07445961425"));
        studentList.add(new Student(1005, "Paul Abraham", "07485661425"));
        studentList.add(new Student(1006, "Dooie Kwong", "07486561425"));
        studentList.add(new Student(1007, "Frank Valdes", "07485331425"));
        studentList.add(new Student(1008, "Chris John", "07485661325"));
        studentList.add(new Student(1009, "Elizabeth Antony", "07495661425"));
    }

    public ArrayList<Timetable> getTimetableList() {
        return timetableList;
    }

    public ArrayList<Student> getStudentList() {
        return studentList;
    }

    public int addToBooking(int studentId, int lessonId) {
        for (Integer temBookingId : bookingData.keySet()) {
            int tempStudentId = bookingData.get(temBookingId).get(0);
            int tempLessonId = bookingData.get(temBookingId).get(1);
            if (tempLessonId == lessonId && tempStudentId == studentId) {
                return -1;
            }
        }
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(studentId);
        arrayList.add(lessonId);
        bookingId = bookingId + 1;
        bookingData.put(bookingId, arrayList);
        return bookingId;
    }

    public ArrayList<Integer> getBooking(int bookingId) {
        ArrayList<Integer> arrayList;
        arrayList = bookingData.get(bookingId);
        return arrayList;
    }

    public boolean cancelBooking(int bookingId) {
        if (bookingData.containsKey(bookingId)) {
            bookingData.remove(bookingId);
            return true;
        }
        return false;
    }

    public void addRating(int lessonId, int rating) {
        ArrayList<Integer> arrayList;
        if (ratingData.containsKey(lessonId)) {
            arrayList = ratingData.get(lessonId);
            arrayList.add(rating);
        } else {
            arrayList = new ArrayList<>();
            arrayList.add(rating);
        }
        ratingData.put(lessonId, arrayList);
    }

    public void addReview(int lessonId, String review) {
        ArrayList<String> arrayList;
        if (reviewData.containsKey(lessonId)) {
            arrayList = reviewData.get(lessonId);
            arrayList.add(review);
        } else {
            arrayList = new ArrayList<>();
            arrayList.add(review);
        }
        reviewData.put(lessonId, arrayList);
    }

    private double getTotalRating(int lessonId) {
        int rating = 0;
        if (ratingData.containsKey(lessonId)) {
            ArrayList<Integer> list = ratingData.get(lessonId);
            for (int i = 0; i < list.size(); i++) {
                rating = rating + list.get(i);
            }
        }
        return rating;
    }

    private double getTotalRatingCount(int lessonId) {
        int rating = 0;
        if (ratingData.containsKey(lessonId)) {
            ArrayList<Integer> list = ratingData.get(lessonId);
           rating = list.size();
        }
        return rating;
    }

    public HashMap<String, ArrayList<Double>> getMonthlyReport(int month) {
        HashMap<String, ArrayList<Double>> report = new HashMap<>();
        HashMap<String, Double> students = new HashMap<>();
        HashMap<String, Double> review = new HashMap<>();
        HashMap<String, Double> reviewCount = new HashMap<>();
        for (int i = 0; i < timetableList.size(); i++) {
            Timetable item = timetableList.get(i);
            if (item.getMonth() == month) {
                if (students.containsKey(item.getExercise())) {
                    students.put(item.getExercise().toString(), students.get(item.getExercise().toString()) + item.getBookedCount());
                } else {
                    students.put(item.getExercise().toString(), (double) item.getBookedCount());
                }

                if (reviewCount.containsKey(item.getExercise())) {
                    reviewCount.put(item.getExercise().toString(), reviewCount.get(item.getExercise().toString()) + getTotalRatingCount(item.getLessonId()));
                } else {
                    reviewCount.put(item.getExercise().toString(), getTotalRatingCount(item.getLessonId()));
                }

                if (review.containsKey(item.getExercise())) {
                    review.put(item.getExercise().toString(), review.get(item.getExercise().toString()) + getTotalRating(item.getLessonId()));
                } else {
                    review.put(item.getExercise().toString(), getTotalRating(item.getLessonId()));
                }
            }
        }
        for (String exercise : students.keySet()) {
            ArrayList<Double> temp = new ArrayList<>();
            temp.add(students.get(exercise) + reviewCount.get(exercise));
            temp.add(review.get(exercise));
            temp.add(reviewCount.get(exercise));
            if (reviewCount.get(exercise) > 0) {
                temp.add(review.get(exercise) / reviewCount.get(exercise));
            } else {
                temp.add((double) 0);
            }
            temp.add((double) (reviewCount.get(exercise) * price.get(exercise)));
            report.put(exercise, temp);
        }
        return report;
    }

    public boolean isValidBookingID(int bookingId) {
        return bookingData.containsKey(bookingId);
    }

    public Timetable getLesson(int lessonId) {
        Timetable timetable = new Timetable(lessonId);
        if (timetableList.contains(timetable)) {
            int index = timetableList.indexOf(timetable);
            timetable = timetableList.get(index);
        }
        return timetable;
    }

    public int getBkdStudentId(int bookingId) {
        ArrayList<Integer> list = bookingData.get(bookingId);
        return list.get(0);
    }

    public int modifyBooking(int bookingId, int studentId, int lessonId) {
        for (Integer temBookingId : bookingData.keySet()) {
            int tempStudentId = bookingData.get(temBookingId).get(0);
            int tempLessonId = bookingData.get(temBookingId).get(1);
            if (tempLessonId == lessonId && tempStudentId == studentId) {
                return -1;
            }
        }
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(studentId);
        arrayList.add(lessonId);
        bookingData.put(bookingId, arrayList);
        return 1;
    }

    public Map<String, Double> championReport(int month) {
        HashMap<String, ArrayList<Double>> report = SingletonData.getInstance().getMonthlyReport(month);
        Map<String, Double> championReport = new HashMap<>();
        for (String exercise : report.keySet()) {
            championReport.put(exercise, Double.valueOf(String.format("%.2f",report.get(exercise).get(4))));
        }

        championReport = championReport.entrySet().
                stream().
                sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).
                collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        return championReport;
    }

    public enum Slot {
        MORNING("MORNING"),
        AFTERNOON("AFTERNOON"),
        EVENING("EVENING");

        private final String text;

        Slot(String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    public enum Day {
        SATURDAY("SATURDAY"),
        SUNDAY("SUNDAY");
        private final String day;

        Day(String day) {
            this.day = day;
        }

        @Override
        public String toString() {
            return day;
        }
    }

    public enum Exercise {
        YOGA("YOGA"),
        ZUMBA("ZUMBA"),
        AQUACISE("AQUACISE"),
        BOX_FIT("BOX FIT"),
        BODY_BLITZ("BODY BLITZ");
        private final String exercise;

        Exercise(String exercise) {
            this.exercise = exercise;
        }

        @Override
        public String toString() {
            return exercise;
        }
    }

    public enum Rating {
        VERY_DISSATISFIED("1: VERY DISSATISFIED"),
        DISSATISFIED("2: DISSATISFIED"),
        OK("3: OK"),
        SATISFIED("4: SATISFIED"),
        VERY_SATISFIED("5: VERY SATISFIED");
        private final String exercise;

        Rating(String exercise) {
            this.exercise = exercise;
        }

        @Override
        public String toString() {
            return exercise;
        }
    }

    public int getBookingId() {
        return bookingId;
    }

    public HashMap<Integer, ArrayList<Integer>> getBookingData() {
        return bookingData;
    }

    public HashMap<Integer, ArrayList<Integer>> getRatingData() {
        return ratingData;
    }
}
