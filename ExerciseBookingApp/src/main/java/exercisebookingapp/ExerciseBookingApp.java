/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package exercisebookingapp;

import exercisebookingapp.model.Booking;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Snehajeffy
 */
public class ExerciseBookingApp {

    /**
     * @param args the command line arguments
     */
    private static ArrayList<Booking> bookingList = new ArrayList<>();

    public void entryPage() {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("*************************************************");
            System.out.println("*    Welcome to USC Exercise Booking System!    *");
            System.out.println("*************************************************");
            System.out.println("* 1. Book a group exercise lesson               *");
            System.out.println("* 2. Change Booking                             *");
            System.out.println("* 3. Write review and rating of lesson attended *");
            System.out.println("* 4. View monthly lesson report                 *");
            System.out.println("* 5. View monthly champion exercise report      *");
            System.out.println("* 6. Exit                                       *");
            System.out.println("*************************************************");
            System.out.println("\r\rPlease note we have only Saturday and Sunday exercise lessons");
            System.out.println("in Morning, Afternoon and Evening");
            System.out.println("Choose your option from the menu: ");
            int entry = scanner.nextInt();
            switch (entry) {
                case 1:
                    System.out.println("Book a group exercise lesson");
                    BookLesson bookLesson = new BookLesson();
                    bookLesson.selectTimeTable();
                    break;
                case 2:
                    System.out.println("Change Booking");
                    ModifyBooking modifyBooking = new ModifyBooking();
                    modifyBooking.modifyBooking();
                    break;
                case 3:
                    System.out.println("Write review and rating of lesson attended");
                    RateLesson rating = new RateLesson();
                    rating.rateLesson();
                    break;
                case 4:
                    System.out.println("Monthly lesson report");
                    MonthlyReport report= new MonthlyReport();
                    report.monthlyLessonReport();
                    break;
                case 5:
                    System.out.println("Monthly champion exercise report");
                    MonthlyReport champion= new MonthlyReport();
                    champion.championReport();
                    break;
                case 6:
                    System.exit(0);
                    break;
                default:
//                    System.out.print("Everything on the console will cleared");
//                    System.out.print("\033[H\033[2J");
//                    System.out.flush();
                    System.out.println("Sorry Wrong Input!");
                    System.out.println("Please choose your option(1-6)");
            }
        }
    }

    public static void main(String[] args) {
        ExerciseBookingApp app = new ExerciseBookingApp();
        app.entryPage();
    }
}

