/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package exercisebookingapp.model;

import java.util.Objects;

/**
 *
 * @author Snehajeffy
 */
public class Student {

    private int studentId;
    private String name;
    private String phone;

    public Student(int studentId) {
        //set object properties from the arguments/parameters
        this.studentId = studentId;
    }
    public Student(int studentId, String name, String phone) {
        //set object properties from the arguments/parameters
        this.studentId = studentId;
        this.name = name;
        this.phone = phone;

    }

    public int getStudentId() {
        return studentId;
    }

    public String getName() {
        return name;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    @Override
    public String toString() {
        return  "StudentId=" + studentId +
                ", name='" + name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return studentId == student.studentId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentId);
    }
}
