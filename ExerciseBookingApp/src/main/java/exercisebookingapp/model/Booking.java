/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package exercisebookingapp.model;

/**
 * @author Snehajeffy
 */
public class Booking {

//    public Booking(int bookingId, int slotId, int studentId, int bookingStatus, String lessonReview, int lessonRating) {
//        //set object properties from the arguments/parameters
//        this.bookingId = bookingId;
//        this.slotId = slotId;
//        this.studentId = studentId;
//        this.bookingStatus = bookingStatus;
//        this.lessonReview = lessonReview;
//        this.lessonRating = lessonRating;
//    }


    /**
     * @return the bookingId
     */
    public int getBookingId() {
        return bookingId;
    }

    /**
     * @param bookingId the bookingId to set
     */
    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    /**
     * @return the slotId
     */
    public int getSlotId() {
        return slotId;
    }

    /**
     * @param slotId the slotId to set
     */
    public void setSlotId(int slotId) {
        this.slotId = slotId;
    }

    /**
     * @return the studentId
     */
    public int getStudentId() {

        return studentId;
    }

    /**
     * @param studentId the studentId to set
     */
    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    /**
     * @return the bookingStatus
     */
    public int getBookingStatus() {
        return bookingStatus;
    }

    /**
     * @param bookingStatus the bookingStatus to set
     */
    public void setBookingStatus(int bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    /**
     * @return the lessonReview
     */
    public String getLessonReview() {
        return lessonReview;
    }

    /**
     * @param lessonReview the lessonReview to set
     */
    public void setLessonReview(String lessonReview) {
        this.lessonReview = lessonReview;
    }

    /**
     * @return the lessonRating
     */
    public int getLessonRating() {
        return lessonRating;
    }

    /**
     * @param lessonRating the lessonRating to set
     */
    public void setLessonRating(int lessonRating) {
        this.lessonRating = lessonRating;
    }

    private int bookingId;
    private int slotId;
    private int studentId;
    private int bookingStatus;

    private String lessonReview;
    private int lessonRating;
}
