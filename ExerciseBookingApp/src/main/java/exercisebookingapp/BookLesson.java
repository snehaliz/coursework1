package exercisebookingapp;

import exercisebookingapp.data.SingletonData;
import exercisebookingapp.model.Student;
import exercisebookingapp.model.Timetable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class BookLesson {

    public void selectTimeTable() {
        System.out.println("1: DAY");
        System.out.println("2: LESSON NAME");
        System.out.println("3: EXIT");
        System.out.print("Choose an option: ");
        Scanner sc = new Scanner(System.in);
        int option = sc.nextInt();
        switch (option) {
            case 1:
                System.out.println("*****TIME TABLE BY DAY*****");
                byDay();
                break;
            case 2:
                System.out.println("*****TIME TABLE BY NAME*****");
                byName();
                break;
            case 3:
                return;
            default:
                System.out.println("WRONG INPUT!!!");

        }
    }

    void byDay() {
            System.out.println("1: SATURDAY");
            System.out.println("2: SUNDAY");
            System.out.print("Choose a day: ");
            Scanner sc = new Scanner(System.in);
            int option = sc.nextInt();
            switch (option) {
                case 1:
                    saturday();
                    break;
                case 2:
                    sunday();
                    break;
                default:
                    System.out.println("WRONG INPUT!!!");
            }
    }

    void saturday() {
        System.out.println("* * * * * * SATURDAY  LESSONS * * * * * *");
        System.out.println("* * * * * * * * * * * * * * * * * * * * *");
        ArrayList<Timetable> list = SingletonData.getInstance().getTimetableList();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getDay().toString().equalsIgnoreCase(SingletonData.Day.SATURDAY.toString())) {
                System.out.println(list.get(i).toString());
            }
        }
        System.out.println("Please enter the exercise number for booking or enter 0 to go to main menu: ");
        Scanner scanner = new Scanner(System.in);
        int opt = scanner.nextInt();
        Timetable timetable = new Timetable(opt);
        if (list.contains(timetable)) {
            confirmBooking(opt);
        } else if (opt == 0) {
            selectTimeTable();
        } else {
            System.out.println("WRONG INPUT");
            saturday();
        }
    }


    void sunday() {
        System.out.println("* * * * * * * SUNDAY  LESSONS * * * * * * *");
        System.out.println("* * * * * * * * * * * * * * * * * * * * * *");
        ArrayList<Timetable> list = SingletonData.getInstance().getTimetableList();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getDay().toString().equalsIgnoreCase(SingletonData.Day.SUNDAY.toString())) {
                System.out.println(list.get(i).toString());
            }
        }
        System.out.println("Please enter the exercise number for booking or enter 0 to go to main menu: ");
        Scanner scanner = new Scanner(System.in);
        int opt = scanner.nextInt();
        Timetable timetable = new Timetable(opt);
        if (list.contains(timetable)) {
            confirmBooking(opt);
        } else if (opt == 0) {
            selectTimeTable();
        } else {
            System.out.println("WRONG INPUT");
            sunday();
        }
    }

    void byName() {
        Scanner op = new Scanner(System.in);
        System.out.print("Please enter the exercise name: ");
        System.out.println(Arrays.toString(SingletonData.Exercise.values()));
        String name = op.nextLine();
        ArrayList<Timetable> list = SingletonData.getInstance().getTimetableList();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getExercise().equalsIgnoreCase(name)) {
                System.out.println(list.get(i).toString());
            }
        }
        System.out.println("Please enter the exercise number for booking or enter 0 to go to main menu: ");
        Scanner scanner = new Scanner(System.in);
        int opt = scanner.nextInt();
        Timetable timetable = new Timetable(opt);
        if (list.contains(timetable)) {
            confirmBooking(opt);
        } else if (opt == 0) {
            selectTimeTable();
        } else {
            System.out.println("WRONG INPUT");
            byName();
        }

    }

    private void confirmBooking(int opt) {
        System.out.println("* * * * * * STUDENT  DETAILS * * * * * *");
        System.out.println("* * * * * * * * * * * * * * * * * * * * *");
        ArrayList<Student> list = SingletonData.getInstance().getStudentList();
        ArrayList<Timetable> timetableList = SingletonData.getInstance().getTimetableList();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).toString());
        }
        System.out.println("Please enter the student ID for booking or enter 0 to go to main menu: ");
        Scanner scanner = new Scanner(System.in);
        int studentId = scanner.nextInt();
        Student temp = new Student(studentId);
        Timetable timetable = new Timetable(opt);
        if (list.contains(temp)) {
            int index = list.indexOf(temp);
            temp = list.get(index);
            index = timetableList.indexOf(timetable);
            timetable = timetableList.get(index);
            if (timetable.getBookedCount() < 4) {
                timetable = timetableList.get(index);
                int bookingId = SingletonData.getInstance().addToBooking(studentId, opt);
                if (bookingId != -1) {
                    SingletonData.getInstance().getTimetableList().get(index).setBookedCount(timetable.getBookedCount() + 1);
                    System.out.println("* * * * * * BOOKING  SUCCESSFUL * * * * * *");
                    System.out.println("* * * * * * * * * DETAILS * * * * * * * * *");
                    System.out.println("* * * * * * * * * * * * * * * * * * * * * *");
                    System.out.println("\r\rBOOKING ID --->> " + bookingId);
                    System.out.println("Lesson Details --->> " + timetable.toString());
                    System.out.println("Student Details --->> " + temp.toString());
                    System.out.println("* * * * * * * * * * * * * * * * * * * * *");
                    System.out.println("\r\r\rDo you want to continue(y/n)?");
                    char option = scanner.next().charAt(0);
                    if (option == 'Y' || option == 'y') {
                        return;
                    } else {
                        System.exit(0);
                    }
                } else {
                    System.out.println("\r\r\r* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
                    System.out.println("* * * * Student has already booked the lesson * * * * * * *");
                    System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
                    System.out.println("\r\r\rDo you want modify the booking(y/n)?");
                    char option = scanner.next().charAt(0);
                    if (option == 'Y' || option == 'y') {
                        confirmBooking(opt);
                    } else {
                        return;
                    }
                }
            } else {
                System.out.println("SEAT FULL...");
                System.out.println("Press 1 to change the lesson and 0 to goto main menu");
                int option = scanner.nextInt();
                if (option == 1) {
                    selectTimeTable();
                } else {
                    return;
                }
            }
        } else {
            System.out.println("WRONG INPUT");
            confirmBooking(opt);
        }
    }
}
