package exercisebookingapp;

import exercisebookingapp.data.SingletonData;
import exercisebookingapp.model.Timetable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MonthlyReport {
    public void monthlyLessonReport() {
        System.out.println("\r\r\r* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
        System.out.println("* * * * * * * * * * * Monthly  Report * * * * * * * * * * *");
        System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");

        System.out.println("\r\r\rPlease enter the month[1-12] or enter 0 to go to main menu: ");
        Scanner scanner = new Scanner(System.in);
        int month = scanner.nextInt();
        if (month >= 1 && month <= 12) {
            System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
            System.out.println("*       Exercise            No of Bookings         Avg. Rating       *");
            HashMap<String, ArrayList<Double>> report = SingletonData.getInstance().getMonthlyReport(month);
            for (String exercise : report.keySet()) {
                System.out.println("      " + exercise + "          " + report.get(exercise).get(0)+ "          " + report.get(exercise).get(3)+ "          " );

            }
            System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
        } else {
            return;
        }
    }

    public void championReport() {
        System.out.println("\r\r\r* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
        System.out.println("* * * * * * * * * * * Champion  Exercise * * * * * * * * * * *");
        System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");

        System.out.println("\r\r\rPlease enter the month[1-12] or enter 0 to go to main menu: ");
        Scanner scanner = new Scanner(System.in);
        int month = scanner.nextInt();
        if (month >= 1 && month <= 12) {
            System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
            System.out.println("*       Exercise                    Income                *");
            Map<String, Double> report = SingletonData.getInstance().championReport(month);

            for (String exercise : report.keySet()) {
                System.out.println("      " + exercise + "           " + report.get(exercise));

            }
            System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
        } else {
            return;
        }
    }
}
