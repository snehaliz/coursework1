package exercisebookingapp;

import exercisebookingapp.data.SingletonData;
import exercisebookingapp.model.Timetable;

import java.util.ArrayList;
import java.util.Scanner;

public class MonthlyReport {
    public void monthlyLessonReport() {
        System.out.println("\r\r\r* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
        System.out.println("* * * * * * * * * * * Monthly  Report * * * * * * * * * * *");
        System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");

        System.out.println("\r\r\rPlease enter the month[1-12] or enter 0 to go to main menu: ");
        Scanner scanner = new Scanner(System.in);
        int month = scanner.nextInt();
        if (month >= 1 && month <= 12) {
            System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
            System.out.println("*       Exercise            Week         No of Bookings       *");
            ArrayList<Timetable> list = SingletonData.getInstance().getTimetableList();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getMonth() == month) {
                    System.out.println("      " + list.get(i).getExercise().toString() + "          " + list.get(i).getWeek()+ "          " + list.get(i).getBookedCount());
                }
            }
            System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
        } else {
            return;
        }
    }
}
