package exercisebookingapp;

import exercisebookingapp.data.SingletonData;
import exercisebookingapp.model.Student;
import exercisebookingapp.model.Timetable;

import java.util.ArrayList;
import java.util.Scanner;

public class ModifyBooking {
    public void modifyBooking() {
        System.out.println("* * * * * * MODIFY  BOOKING * * * * * *");
        System.out.println("* * * * * * * * * * * * * * * * * * * *");
        System.out.println("Please enter the Booking ID to modify booking or enter 0 to go to main menu: ");
        Scanner scanner = new Scanner(System.in);
        int bookingId = scanner.nextInt();
        if (SingletonData.getInstance().isValidBookingID(bookingId)) {
            ArrayList<Integer> list = SingletonData.getInstance().getBooking(bookingId);
            Timetable timetable = SingletonData.getInstance().getLesson(list.get(1));
            System.out.println("* * * * * * * BOOKED  LESSONS * * * * * * *");
            System.out.println("* * * * * * * * * * * * * * * * * * * * * *");
            System.out.println(timetable.toString());
            System.out.println("* * * * * * * * * * * * * * * * * * * * * *");

            System.out.println("\r\r\r1: Cancel the booking");
            System.out.println("2: Modify the booking");
            System.out.println("3: Exit");
            System.out.print("Choose an option: ");
            Scanner sc = new Scanner(System.in);
            int option = sc.nextInt();
            switch (option) {
                case 1:
                    cancelBooking(bookingId, timetable.getLessonId());
                    break;
                case 2:
                    modifyExercise(bookingId);
                    break;
                case 3:
                    return;
                default:
                    System.out.println("WRONG INPUT!!!");
                    modifyBooking();

            }
        } else if (bookingId == 0) {
            return;
        } else {
            System.out.println("WRONG INPUT");
            modifyBooking();
        }
    }

    private void modifyExercise(int bookingId) {
        ArrayList<Timetable> list = SingletonData.getInstance().getTimetableList();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).toString());
        }
        System.out.println("Please enter the exercise number for booking or enter 0 to go to main menu: ");
        Scanner scanner = new Scanner(System.in);
        int opt = scanner.nextInt();
        Timetable timetable = new Timetable(opt);
        if (list.contains(timetable)) {
            confirmBooking(opt,bookingId);
        } else if (opt == 0) {
            return;
        } else {
            System.out.println("WRONG INPUT");
            modifyBooking();
        }
    }
    private void confirmBooking(int opt, int bookingId) {
        ArrayList<Student> list = SingletonData.getInstance().getStudentList();
        ArrayList<Timetable> timetableList = SingletonData.getInstance().getTimetableList();
        Scanner scanner = new Scanner(System.in);
        int studentId = SingletonData.getInstance().getBkdStudentId(bookingId);
        Student temp = new Student(studentId);
        Timetable timetable = new Timetable(opt);
        if (list.contains(temp)) {
            int index = list.indexOf(temp);
            temp = list.get(index);
            index = timetableList.indexOf(timetable);
            timetable = timetableList.get(index);
            if (timetable.getBookedCount() < 4) {
                timetable = timetableList.get(index);
                int status = SingletonData.getInstance().modifyBooking(bookingId,studentId, opt);
                if (status != -1) {
                    SingletonData.getInstance().getTimetableList().get(index).setBookedCount(timetable.getBookedCount() + 1);
                    System.out.println("* * * * * * BOOKING  SUCCESSFUL * * * * * *");
                    System.out.println("* * * * * * * * * DETAILS * * * * * * * * *");
                    System.out.println("* * * * * * * * * * * * * * * * * * * * * *");
                    System.out.println("\r\rBOOKING ID --->> " + bookingId);
                    System.out.println("Lesson Details --->> " + timetable.toString());
                    System.out.println("Student Details --->> " + temp.toString());
                    System.out.println("* * * * * * * * * * * * * * * * * * * * *");

                    int oldLessonId = SingletonData.getInstance().getBooking(bookingId).get(1);
                    timetable = new Timetable(oldLessonId);
                    index = timetableList.indexOf(timetable);
                    timetable = timetableList.get(index);
                    SingletonData.getInstance().getTimetableList().get(index).setBookedCount(timetable.getBookedCount() - 1);


                    System.out.println("\r\r\rDo you want to continue(y/n)?");
                    char option = scanner.next().charAt(0);
                    if (option == 'Y' || option == 'y') {
                        return;
                    } else {
                        System.exit(0);
                    }
                } else {
                    System.out.println("\r\r\r* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
                    System.out.println("* * * * Student has already booked the lesson * * * * * * *");
                    System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
                    System.out.println("\r\r\rDo you want modify the booking(y/n)?");
                    char option = scanner.next().charAt(0);
                    if (option == 'Y' || option == 'y') {
                        modifyExercise(bookingId);
                    } else {
                        return;
                    }
                }
            } else {
                System.out.println("SEAT FULL...");
                System.out.println("Press 1 to change the lesson and 0 to goto main menu");
                int option = scanner.nextInt();
                if (option == 1) {
                    modifyExercise(bookingId);
                } else {
                    return;
                }
            }
        } else {
            System.out.println("WRONG INPUT");
            modifyBooking();
        }
    }
    private void cancelBooking(int bookingId, int lessonId) {
        boolean result = SingletonData.getInstance().cancelBooking(bookingId);
        if (result) {
            ArrayList<Timetable> timetableList = SingletonData.getInstance().getTimetableList();
            Timetable timetable = new Timetable(lessonId);
            int index = timetableList.indexOf(timetable);
            timetable = timetableList.get(index);
            SingletonData.getInstance().getTimetableList().get(index).setBookedCount(timetable.getBookedCount() - 1);

            System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * *");
            System.out.println("* * * * * * BOOKING CANCELED SUCCESSFULLY * * * * * ");
            System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * *");
            System.out.println("\r\r\rDo you want to continue(y/n)?");
            Scanner scanner = new Scanner(System.in);
            char option = scanner.next().charAt(0);
            if (option == 'Y' || option == 'y') {
                return;
            } else {
                System.exit(0);
            }
        } else {
            System.out.println("WRONG INPUT");
            modifyBooking();
        }
    }
}
