package exercisebookingapp;

import exercisebookingapp.data.SingletonData;
import exercisebookingapp.model.Student;
import exercisebookingapp.model.Timetable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class RateLesson {
    public void rateLesson() {
        System.out.println("* * * * * * RATE LESSON * * * * * *");
        System.out.println("* * * * * * * * * * * * * * * * * * ");
        ArrayList<Student> list = SingletonData.getInstance().getStudentList();
        System.out.println("Please enter the Booking ID to rate a lesson or enter 0 to go to main menu: ");
        Scanner scanner = new Scanner(System.in);
        int bookingId = scanner.nextInt();
        if (SingletonData.getInstance().isValidBookingID(bookingId)) {
            int lessonId = SingletonData.getInstance().getBooking(bookingId).get(1);
            Student temp = new Student(SingletonData.getInstance().getBkdStudentId(bookingId));
            if (list.contains(temp)) {
                Timetable timetables = SingletonData.getInstance().getLesson(lessonId);
                System.out.println("* * * * * * * BOOKED  LESSON * * * * * * *");
                System.out.println("* * * * * * * * * * * * * * * * * * * * * *");
                System.out.println(timetables);
                System.out.println("* * * * * * * * * * * * * * * * * * * * * *");
                rateExercise(lessonId);
            } else if (bookingId == 0) {
                return;
            } else {
                System.out.println("WRONG INPUT");
                rateLesson();
            }
        }
    }

    private void rateExercise(int lessonID) {
        Scanner op = new Scanner(System.in);
        System.out.print("Please rate the exercise:");
        System.out.print(Arrays.toString(SingletonData.Rating.values()));
        int rating = op.nextInt();
        if (rating >= 1 && rating <= 5) {
            ArrayList<Timetable> timetableList = SingletonData.getInstance().getTimetableList();
            Timetable timetable = new Timetable(lessonID);
            int index = timetableList.indexOf(timetable);
            timetable = timetableList.get(index);


            SingletonData.getInstance().addRating(timetable.getExercise(), rating);

            System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * *");
            System.out.println("* * * * * * RATE EXERCISE SUCCESSFULLY * * * * *  * ");
            System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * *");
            System.out.println("\r\r\rDo you want to continue(y/n)?");
            Scanner scanner = new Scanner(System.in);
            char option = scanner.next().charAt(0);
            if (option == 'Y' || option == 'y') {
                return;
            } else {
                System.exit(0);
            }
        } else {
            System.out.println("WRONG INPUT");
            rateLesson();
        }
    }
}
