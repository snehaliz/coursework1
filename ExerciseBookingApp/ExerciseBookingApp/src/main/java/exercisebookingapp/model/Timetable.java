/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package exercisebookingapp.model;

import exercisebookingapp.data.SingletonData;

import java.util.Objects;

/**
 *
 * @author Snehajeffy
 */
public class Timetable {

    public Timetable(int lessonId){
        this.lessonId = lessonId;
    }
    public Timetable(int lessonId, int month, int week, SingletonData.Day day, SingletonData.Slot slot, SingletonData.Exercise exercise, float price) {
        //set object properties from the arguments/parameters
        this.lessonId = lessonId;
        this.month = month;
        this.week = week;
        this.day = day;
        this.slot = slot;
        this.exercise = exercise;
        this.price = price;

    }

    /**
     * @return the week
     */
    public int getWeek() {
        return week;
    }

    /**
     * @return the day
     */
    public SingletonData.Day getDay() {
        return day;
    }

    /**
     * @return the slot
     */
    public SingletonData.Slot getSlot() {
        return slot;
    }

    /**
     * @return the exercise
     */
    public String getExercise() {
        return exercise.toString();
    }

    /**
     * @return the price
     */
    public float getPrice() {
        return price;
    }

    public int getBookedCount() {
        return bookedCount;
    }

    public void setBookedCount(int bookedCount) {
        this.bookedCount = bookedCount;
    }

    public int getLessonId() {
        return lessonId;
    }

    public int getMonth() {
        return month;
    }

    private int lessonId;
    private int month;
    private int week;
    private SingletonData.Day day;
    private SingletonData.Slot slot;
    private SingletonData.Exercise exercise;
    private float price;

    private int bookedCount;

    @Override
    public String toString() {
        return  "LessonId=" + lessonId +
                ", Month=" + month +
                ", Week=" + week +
                ", Day=" + day.toString() +
                ", Slot=" + slot.toString() +
                ", Exercise='" + exercise.toString() +
                ", Price=" + price +
                ", No of bookings=" + bookedCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Timetable timetable = (Timetable) o;
        return lessonId == timetable.lessonId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(lessonId);
    }
}
